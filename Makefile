GIMP_VER = 2.4

PREFIX      = /usr
INSTALLDIR  = $(PREFIX)/lib/gimp/2.0/modules
#INSTALLDIR = $(HOME)/.gimp-$(GIMP_VER)/modules

ifdef HAVE_LCMS1
  PKGNAME_LCMS  = lcms
  CFLAGS       += -DHAVE_LCMS1
else
  PKGNAME_LCMS  = lcms2
  CFLAGS       += -DHAVE_LCMS2
endif

INCLUDES = `pkg-config --cflags gtk+-2.0 gimp-2.0 gimpui-2.0 $(PKGNAME_LCMS)`
LIBS     = `pkg-config --libs gtk+-2.0 gimp-2.0 gimpui-2.0 $(PKGNAME_LCMS)`

GETTEXT_PACKAGE = gimp20-display-filter-lcms

CC      = gcc -g
DEPEND  = gccmakedep
RM      = rm -f
MAKE    = make PREFIX="$(PREFIX)"

.SUFFIXES: .c .o


NLS      = -DENABLE_NLS -DGETTEXT_PACKAGE="\"$(GETTEXT_PACKAGE)\""
CFLAGS  += $(INCLUDES) $(NLS) -fPIC
LDFLAGS += --shared 
#LIBS   +=

SOURCES = display-filter-lcms.c

OBJECTS = $(SOURCES:.c=.o)


TARGETS = lib$(SOURCES:.c=.so)


.PHONY: all
all: filters


clean:
	$(RM) *.o *.so


filters: $(TARGETS)

libdisplay-filter-lcms.so: $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@ $(LIBS)


# Inference rules
.c.o:
	$(CC) $(CFLAGS) -c $<


#
#display-filter-lcms.o: enums.h
