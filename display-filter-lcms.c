/* GIMP - The GNU Image Manipulation Program
 * Copyright (C) 1995-1997 Spencer Kimball and Peter Mattis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>  /* lcms.h uses the "inline" keyword */

#include <string.h>

#ifdef G_OS_WIN32
#define STRICT
#include <windows.h>
#define LCMS_WIN_TYPES_ALREADY_DEFINED
#endif

#ifdef HAVE_LCMS1
#include <lcms.h>
#define cmsColorSpaceSignature icColorSpaceSignature
#define cmsSigRgbData icSigRgbData
#define cmsSigCmykData icSigCmykData
#define cmsSigGrayData icSigGrayData
#define cmsSigOutputClass icSigOutputClass
#define LCMSDWORD DWORD
#else
#include <lcms2.h>
#define LCMSDWORD cmsUInt32Number
#endif

#include <gtk/gtk.h>

#ifdef GDK_WINDOWING_QUARTZ
#include <Carbon/Carbon.h>
#include <ApplicationServices/ApplicationServices.h>
#include <CoreServices/CoreServices.h>
#endif

#include "libgimp/gimp.h"
#include "libgimpbase/gimpbase.h"
#include "libgimpcolor/gimpcolor.h"
#include "libgimpconfig/gimpconfig.h"
#include "libgimpmath/gimpmath.h"
#include "libgimpmodule/gimpmodule.h"
#include "libgimpwidgets/gimpwidgets.h"

#include "libgimp/libgimp-intl.h"

#include "devicelink_profiles.h"

#if (GIMP_MAJOR_VERSION > 2 || (GIMP_MAJOR_VERSION == 2 && GIMP_MINOR_VERSION > 6))
#define USE_CAIRO_SURFACE
#endif

/****** error handler ******/
#if !defined HAVE_LCMS1 && defined DEBUG
void handler(cmsContext contextID, cmsUInt32Number ErrorCode, const char *Text);

void handler(cmsContext contextID, cmsUInt32Number ErrorCode, const char *Text)
{
  g_printerr("LCMS Error! %d : %s¥n", ErrorCode, Text);
}
#endif

/****** Devicelink profiles ******/
static cmsHPROFILE preview1[6], preview2[6];

void
devicelink_init ()
{
  cmsHPROFILE p;
#ifdef HAVE_LCMS1
  LPGAMMATABLE on, off, transfer_functions[4];

  on  = cmsBuildGamma (2, 1.0);
  off = cmsAllocGamma (2);
  off->GammaTable[1] = 0;
#else
  cmsToneCurve *on, *off, *transfer_functions[4];
  cmsUInt16Number entries[2] = { 0, 0 };

  on  = cmsBuildGamma (NULL, 1.0);
  off = cmsBuildTabulatedToneCurve16 (NULL, 2, entries);
#endif

  preview2[0] = cmsOpenProfileFromMem (c_gray_profile, sizeof (c_gray_profile));
  preview2[1] = cmsOpenProfileFromMem (m_gray_profile, sizeof (m_gray_profile));
  preview2[2] = cmsOpenProfileFromMem (y_gray_profile, sizeof (y_gray_profile));
  preview2[3] = cmsOpenProfileFromMem (k_gray_profile, sizeof (k_gray_profile));

  transfer_functions[1] = transfer_functions[2] = transfer_functions[3] = off;
  transfer_functions[0] = on;
  preview1[0] = cmsCreateLinearizationDeviceLink (cmsSigCmykData, transfer_functions);

  transfer_functions[0] = transfer_functions[2] = transfer_functions[3] = off;
  transfer_functions[1] = on;
  preview1[1] = cmsCreateLinearizationDeviceLink (cmsSigCmykData, transfer_functions);

  transfer_functions[0] = transfer_functions[1] = transfer_functions[3] = off;
  transfer_functions[2] = on;
  preview1[2] = cmsCreateLinearizationDeviceLink (cmsSigCmykData, transfer_functions);

  transfer_functions[0] = transfer_functions[1] = transfer_functions[2] = off;
  transfer_functions[3] = on;
  preview1[3] = cmsCreateLinearizationDeviceLink (cmsSigCmykData, transfer_functions);

  transfer_functions[0] = transfer_functions[1] = transfer_functions[2] = on;
  transfer_functions[3] = off;
  preview1[4] = preview2[4] =
    cmsCreateLinearizationDeviceLink (cmsSigCmykData, transfer_functions);

  /* preview*[5] has no effects */
  transfer_functions[0] = transfer_functions[1] = transfer_functions[2] = transfer_functions[3] = on;
  preview1[5] = preview2[5] =
    cmsCreateLinearizationDeviceLink (cmsSigCmykData, transfer_functions);

#ifdef HAVE_LCMS1
  cmsFreeGamma (on);
  cmsFreeGamma (off);
#else
  cmsFreeToneCurve (on);
  cmsFreeToneCurve (off);
#endif
}

void
devicelink_free ()
{
  int i;

  for (i = 0; i < 4; i++)
    {
      cmsCloseProfile (preview1[i]);
      cmsCloseProfile (preview2[i]);
    }

  cmsCloseProfile (preview1[4]);
  cmsCloseProfile (preview1[5]);
}
/****** Devicelink profiles ******/


/************* Enums *************/
#define CDISPLAY_LCMS_TYPE_PROOF_CHANNEL \
  (cdisplay_lcms_proof_channel_get_type ())

GType cdisplay_lcms_proof_channel_get_type (void) G_GNUC_CONST;

typedef enum
{
  CDISPLAY_LCMS_PROOF_CHANNEL_C    = 0,
  CDISPLAY_LCMS_PROOF_CHANNEL_M    = 1,
  CDISPLAY_LCMS_PROOF_CHANNEL_Y    = 2,
  CDISPLAY_LCMS_PROOF_CHANNEL_K    = 3,
  CDISPLAY_LCMS_PROOF_CHANNEL_CMY  = 4,
  CDISPLAY_LCMS_PROOF_CHANNEL_CMYK = 5
} CdisplayLcmsProofChannel;


GType
cdisplay_lcms_proof_channel_get_type (void)
{
  static const GEnumValue values[] =
  {
    { CDISPLAY_LCMS_PROOF_CHANNEL_C,    "CDISPLAY_LCMS_PROOF_CHANNEL_C",    "C"    },
    { CDISPLAY_LCMS_PROOF_CHANNEL_M,    "CDISPLAY_LCMS_PROOF_CHANNEL_M",    "M"    },
    { CDISPLAY_LCMS_PROOF_CHANNEL_Y,    "CDISPLAY_LCMS_PROOF_CHANNEL_Y",    "Y"    },
    { CDISPLAY_LCMS_PROOF_CHANNEL_K,    "CDISPLAY_LCMS_PROOF_CHANNEL_K",    "K"    },
    { CDISPLAY_LCMS_PROOF_CHANNEL_CMY,  "CDISPLAY_LCMS_PROOF_CHANNEL_CMY",  "CMY"  },
    { CDISPLAY_LCMS_PROOF_CHANNEL_CMYK, "CDISPLAY_LCMS_PROOF_CHANNEL_CMYK", "CMYK" },
    { 0, NULL, NULL }
  };

  static const GimpEnumDesc descs[] =
  {
    { CDISPLAY_LCMS_PROOF_CHANNEL_C,    "C",    NULL },
    { CDISPLAY_LCMS_PROOF_CHANNEL_M,    "M",    NULL },
    { CDISPLAY_LCMS_PROOF_CHANNEL_Y,    "Y",    NULL },
    { CDISPLAY_LCMS_PROOF_CHANNEL_K,    "K",    NULL },
    { CDISPLAY_LCMS_PROOF_CHANNEL_CMY,  "CMY",  NULL },
    { CDISPLAY_LCMS_PROOF_CHANNEL_CMYK, "CMYK", NULL },
    { 0, NULL, NULL }
  };

  static GType type = 0;

  if (!type)
    {
      type = g_enum_register_static ("CdisplayLcmsProofChannel", values);
      gimp_type_set_translation_domain (type, GETTEXT_PACKAGE);
      gimp_enum_set_value_descriptions (type, descs);
    }

  return type;
}
/************* Enums *************/

#define CDISPLAY_TYPE_LCMS            (cdisplay_lcms_get_type ())
#define CDISPLAY_LCMS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CDISPLAY_TYPE_LCMS, CdisplayLcms))
#define CDISPLAY_LCMS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CDISPLAY_TYPE_LCMS, CdisplayLcmsClass))
#define CDISPLAY_IS_LCMS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CDISPLAY_TYPE_LCMS))
#define CDISPLAY_IS_LCMS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CDISPLAY_TYPE_LCMS))


typedef struct _CdisplayLcms      CdisplayLcms;
typedef struct _CdisplayLcmsClass CdisplayLcmsClass;

struct _CdisplayLcms
{
  GimpColorDisplay  parent_instance;

  cmsHTRANSFORM     transform;

  gboolean          use_color_config;
  gboolean          preserve_pixel;
  GimpColorRenderingIntent intent;
  GimpColorRenderingIntent intent2;
  gboolean          fully_adapted;
  gboolean          use_bpc;
  gboolean          proofing;
  gboolean          black_ink;
  gboolean          media_white;
  CdisplayLcmsProofChannel channel;
  gboolean          grayscale_preview;

  GtkWidget        *frame;
  GtkWidget        *vbox;
  GtkWidget        *hbox_intent;
  GtkWidget        *hbox_channels;
  GtkWidget        *table;
  GtkWidget        *toggle_use_color_config;
  GtkWidget        *toggle_preserve_pixel;
  GtkWidget        *toggle_bpc;
  GtkWidget        *toggle_chad;
  GtkWidget        *toggle_black_ink;
  GtkWidget        *toggle_media_white;
  GtkWidget        *combo_intent;
  GtkWidget        *toggle_grayscale_preview;

  gpointer          weak_pointer;

  gboolean          target_is_cmyk;
  gboolean          update;
};

struct _CdisplayLcmsClass
{
  GimpColorDisplayClass parent_instance;
};


enum
{
  PROP_0,
  PROP_USE_COLOR_CONFIG,
  PROP_PRESERVE_PIXEL,
  PROP_INTENT,
  PROP_INTENT2,
  PROP_FULLY_ADAPTED,
  PROP_USE_BPC,
  PROP_PROOFING,
  PROP_BLACK_INK,
  PROP_MEDIA_WHITE,
  PROP_CHANNEL,
  PROP_GRAYSCALE_PREVIEW
};


GType               cdisplay_lcms_get_type             (void);

static void         cdisplay_lcms_finalize             (GObject           *object);
static void         cdisplay_lcms_get_property         (GObject           *object,
                                                        guint              property_id,
                                                        GValue            *value,
                                                        GParamSpec        *pspec);
static void         cdisplay_lcms_set_property         (GObject           *object,
                                                        guint              property_id,
                                                        const GValue      *value,
                                                        GParamSpec        *pspec);

static GtkWidget  * cdisplay_lcms_configure            (GimpColorDisplay  *display);
#ifdef USE_CAIRO_SURFACE
static void         cdisplay_lcms_convert_surface      (GimpColorDisplay  *display,
                                                        cairo_surface_t   *surface);
#else
static void         cdisplay_lcms_convert              (GimpColorDisplay  *display,
                                                        guchar            *buf,
                                                        gint               width,
                                                        gint               height,
                                                        gint               bpp,
                                                        gint               bpl);
#endif
static void         cdisplay_lcms_changed              (GimpColorDisplay  *display);

static cmsHPROFILE  cdisplay_lcms_get_rgb_profile      (CdisplayLcms      *lcms);
static cmsHPROFILE  cdisplay_lcms_get_display_profile  (CdisplayLcms      *lcms);
static cmsHPROFILE  cdisplay_lcms_get_printer_profile  (CdisplayLcms      *lcms);

static void         cdisplay_lcms_attach_labelled      (GtkTable          *table,
                                                        gint               row,
                                                        const gchar       *text,
                                                        GtkWidget         *widget);
static void         cdisplay_lcms_update_profile_label (CdisplayLcms      *lcms,
                                                        const gchar       *name);
static void         cdisplay_lcms_notify_profile       (GObject           *config,
                                                        GParamSpec        *pspec,
                                                        CdisplayLcms      *lcms);
static gboolean     channel_select                     (cmsHPROFILE        profile,
                                                        gboolean           flags[4]);


static const GimpModuleInfo cdisplay_lcms_info =
{
  GIMP_MODULE_ABI_VERSION,
  N_("Yet another color management display filter"),
  "Sven Neumann, modified by Yoshinori Yamakawa",
  "v0.3",
  "(c) 2005 - 2016, released under the GPL",
  "2005 - 2016"
};

G_DEFINE_DYNAMIC_TYPE (CdisplayLcms, cdisplay_lcms,
                       GIMP_TYPE_COLOR_DISPLAY)

G_MODULE_EXPORT const GimpModuleInfo *
gimp_module_query (GTypeModule *module)
{
  return &cdisplay_lcms_info;
}

G_MODULE_EXPORT gboolean
gimp_module_register (GTypeModule *module)
{
  cdisplay_lcms_register_type (module);

  return TRUE;
}

static void
cdisplay_lcms_class_init (CdisplayLcmsClass *klass)
{
  GObjectClass          *object_class  = G_OBJECT_CLASS (klass);
  GimpColorDisplayClass *display_class = GIMP_COLOR_DISPLAY_CLASS (klass);

  object_class->finalize     = cdisplay_lcms_finalize;
  object_class->get_property = cdisplay_lcms_get_property;
  object_class->set_property = cdisplay_lcms_set_property;

  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_USE_COLOR_CONFIG,
                                    "use-color-config", NULL,
                                    TRUE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_PRESERVE_PIXEL,
                                    "preserve-pixel", NULL,
                                    FALSE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_ENUM (object_class, PROP_INTENT,
                                 "intent", NULL,
                                 GIMP_TYPE_COLOR_RENDERING_INTENT,
                                 GIMP_COLOR_RENDERING_INTENT_PERCEPTUAL,
                                 0);
  GIMP_CONFIG_INSTALL_PROP_ENUM (object_class, PROP_INTENT2,
                                 "intent2", NULL,
                                 GIMP_TYPE_COLOR_RENDERING_INTENT,
                                 GIMP_COLOR_RENDERING_INTENT_PERCEPTUAL,
                                 0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_USE_BPC,
                                    "use-bpc", NULL,
                                    TRUE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_FULLY_ADAPTED,
                                    "fully-adapted", NULL,
                                    TRUE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_PROOFING,
                                    "proofing", NULL,
                                    FALSE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_BLACK_INK,
                                    "simulate-black-ink", NULL,
                                    FALSE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_MEDIA_WHITE,
                                    "simulate-media-white", NULL,
                                    FALSE,
                                    0);
  GIMP_CONFIG_INSTALL_PROP_ENUM (object_class, PROP_CHANNEL,
                                 "channel", NULL,
                                 CDISPLAY_LCMS_TYPE_PROOF_CHANNEL,
                                 CDISPLAY_LCMS_PROOF_CHANNEL_CMYK,
                                 0);
  GIMP_CONFIG_INSTALL_PROP_BOOLEAN (object_class, PROP_GRAYSCALE_PREVIEW,
                                    "grayscale-preview", NULL,
                                    FALSE,
                                    0);
#ifdef INIT_I18N
  INIT_I18N ();
#endif

  display_class->name            = _("Color Management+");
  display_class->help_id         = "gimp-colordisplay-lcms";
  display_class->stock_id        = GIMP_STOCK_DISPLAY_FILTER_LCMS;

  display_class->configure       = cdisplay_lcms_configure;
#ifdef USE_CAIRO_SURFACE
  display_class->convert_surface = cdisplay_lcms_convert_surface;
#else
  display_class->convert         = cdisplay_lcms_convert;
#endif
  display_class->changed         = cdisplay_lcms_changed;

#ifdef HAVE_LCMS1
  cmsErrorAction (LCMS_ERROR_IGNORE);
#elif defined DEBUG
  cmsSetLogErrorHandler(handler);
#endif

  devicelink_init ();
}

static void
cdisplay_lcms_class_finalize (CdisplayLcmsClass *klass)
{
  devicelink_free ();
}

static void
cdisplay_lcms_init (CdisplayLcms *lcms)
{
  lcms->transform = NULL;

  lcms->frame = NULL;
  lcms->vbox = NULL;
  lcms->hbox_intent = NULL;
  lcms->hbox_channels = NULL;
  lcms->table = NULL;
  lcms->toggle_use_color_config = NULL;
  lcms->toggle_preserve_pixel = NULL;
  lcms->toggle_bpc = NULL;
  lcms->toggle_chad = NULL;
  lcms->toggle_black_ink = NULL;
  lcms->toggle_media_white = NULL;
  lcms->combo_intent = NULL;

  lcms->weak_pointer = NULL;

  lcms->target_is_cmyk = TRUE;
  lcms->update = TRUE;
}

static void
cdisplay_lcms_finalize (GObject *object)
{
  CdisplayLcms *lcms = CDISPLAY_LCMS (object);

  if (lcms->transform)
    {
      cmsDeleteTransform (lcms->transform);
      lcms->transform = NULL;
    }

  G_OBJECT_CLASS (cdisplay_lcms_parent_class)->finalize (object);
}

static void
cdisplay_lcms_get_property (GObject    *object,
                            guint       property_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  CdisplayLcms *lcms = CDISPLAY_LCMS (object);

  switch (property_id)
    {
    case PROP_USE_COLOR_CONFIG:
      g_value_set_boolean (value, lcms->use_color_config);
      break;
    case PROP_PRESERVE_PIXEL:
      g_value_set_boolean (value, lcms->preserve_pixel);
      break;
    case PROP_INTENT:
      g_value_set_enum (value, lcms->intent);
      break;
    case PROP_INTENT2:
      g_value_set_enum (value, lcms->intent2);
      break;
    case PROP_FULLY_ADAPTED:
      g_value_set_boolean (value, lcms->fully_adapted);
      break;
    case PROP_USE_BPC:
      g_value_set_boolean (value, lcms->use_bpc);
      break;
    case PROP_PROOFING:
      g_value_set_boolean (value, lcms->proofing);
      break;
    case PROP_BLACK_INK:
      g_value_set_boolean (value, lcms->black_ink);
      break;
    case PROP_MEDIA_WHITE:
      g_value_set_boolean (value, lcms->media_white);
      break;
    case PROP_CHANNEL:
      g_value_set_enum (value, lcms->channel);
      break;
    case PROP_GRAYSCALE_PREVIEW:
      g_value_set_boolean (value, lcms->grayscale_preview);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
cdisplay_lcms_set_property (GObject      *object,
                            guint         property_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  CdisplayLcms *lcms = CDISPLAY_LCMS (object);

  switch (property_id)
    {
    case PROP_USE_COLOR_CONFIG:
      lcms->use_color_config = g_value_get_boolean (value);

      if (lcms->weak_pointer)
        gtk_widget_set_sensitive (lcms->frame, !lcms->use_color_config);

      break;
    case PROP_PRESERVE_PIXEL:
      lcms->preserve_pixel = g_value_get_boolean (value);

      if (lcms->weak_pointer)
        {
          gtk_widget_set_sensitive (lcms->combo_intent, !lcms->preserve_pixel);
          gtk_widget_set_sensitive (lcms->hbox_intent, !lcms->preserve_pixel);
        }
      break;
    case PROP_INTENT:
      lcms->intent = g_value_get_enum (value);

      if (lcms->weak_pointer)
        gtk_widget_set_sensitive (lcms->toggle_chad, lcms->intent == GIMP_COLOR_RENDERING_INTENT_ABSOLUTE_COLORIMETRIC);
      break;
    case PROP_INTENT2:
      lcms->intent2 = g_value_get_enum (value);
      break;
    case PROP_FULLY_ADAPTED:
      lcms->fully_adapted = g_value_get_boolean (value);
      break;
    case PROP_USE_BPC:
      lcms->use_bpc = g_value_get_boolean (value);
      break;
    case PROP_PROOFING:
      lcms->proofing = g_value_get_boolean (value);

      if (lcms->weak_pointer)
          gtk_widget_set_sensitive (lcms->table, lcms->proofing);
      break;
    case PROP_BLACK_INK:
      lcms->black_ink = g_value_get_boolean (value);
      break;
    case PROP_MEDIA_WHITE:
      lcms->media_white = g_value_get_boolean (value);

      if (lcms->weak_pointer)
        gtk_widget_set_sensitive (lcms->toggle_black_ink, lcms->channel == CDISPLAY_LCMS_PROOF_CHANNEL_CMYK && !lcms->media_white);
      break;
    case PROP_CHANNEL:
      lcms->channel = g_value_get_enum (value);

      if (lcms->weak_pointer)
        {
          gboolean flag = !(lcms->channel < CDISPLAY_LCMS_PROOF_CHANNEL_CMYK);

          gtk_widget_set_sensitive (lcms->toggle_black_ink, flag && !lcms->media_white);
          gtk_widget_set_sensitive (lcms->toggle_media_white, flag);
        }
      break;
    case PROP_GRAYSCALE_PREVIEW:
      lcms->grayscale_preview = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }

  if (lcms->update)
    gimp_color_display_changed (GIMP_COLOR_DISPLAY (lcms));
}

#ifdef HAVE_LCMS1
static void
cdisplay_lcms_profile_get_info (cmsHPROFILE   profile,
                                const gchar **name,
                                const gchar **info)
#else
static void
cdisplay_lcms_profile_get_info (cmsHPROFILE   profile,
                                gchar       **name,
                                gchar       **info)
#endif
{
  if (profile)
    {
#ifdef HAVE_LCMS1
      *name = cmsTakeProductDesc (profile);
#else
      cmsUInt32Number  descSize;
      gchar           *descData;

      descSize = cmsGetProfileInfoASCII (profile, cmsInfoDescription,
                                         "en", "US", NULL, 0);
      if (descSize > 0)
        {
          descData = g_new (gchar, descSize + 1);
          descSize = cmsGetProfileInfoASCII (profile, cmsInfoDescription,
                                             "en", "US", descData, descSize);
          if (descSize > 0)
            *name = descData;
          else
            g_free (descData);
        }
#endif

      if (! *name)
#ifdef HAVE_LCMS1
        *name = cmsTakeProductName (profile);
#else
        {
          descSize = cmsGetProfileInfoASCII (profile, cmsInfoModel,
                                             "en", "US", NULL, 0);
          if (descSize > 0)
            {
              descData = g_new (gchar, descSize + 1);
              descSize = cmsGetProfileInfoASCII(profile, cmsInfoModel,
                                                "en", "US", descData, descSize);
              if (descSize > 0)
                *name = descData;
              else
                g_free (descData);
            }
        }
#endif

#ifdef HAVE_LCMS1
      if (*name && ! g_utf8_validate (*name, -1, NULL))
        *name = _("(invalid UTF-8 string)");

      *info = cmsTakeProductInfo (profile);
      if (*name && ! g_utf8_validate (*info, -1, NULL))
        *info = NULL;
#else
      if (*name && ! g_utf8_validate (*name, -1, NULL))
        {
          g_free (*name);
          *name = g_strdup (_("(invalid UTF-8 string)"));
        }

      descSize = cmsGetProfileInfoASCII (profile, cmsInfoManufacturer,
                                         "en", "US", NULL, 0);
      if (descSize > 0)
        {
          descData = g_new (gchar, descSize + 1);
          descSize = cmsGetProfileInfoASCII (profile, cmsInfoManufacturer,
                                             "en", "US", descData, descSize);
          if (descSize > 0)
            *info = descData;
          else
            g_free (descData);
        }

      if (*info && ! g_utf8_validate (*info, -1, NULL))
        {
          g_free (*info);
          *info = NULL;
        }
#endif
    }
  else
    {
#ifdef HAVE_LCMS1
      *name = _("None");
#else
      *name = g_strdup (_("None"));
#endif
      *info = NULL;
    }
}

static GtkWidget *
cdisplay_lcms_configure (GimpColorDisplay *display)
{
  CdisplayLcms *lcms   = CDISPLAY_LCMS (display);
  GObject      *config = G_OBJECT (gimp_color_display_get_config (display));
  GtkWidget    *vbox;
  GtkWidget    *hbox;
  GtkWidget    *table;
  GtkWidget    *label;
  GtkWidget    *checkbutton;
  gint          row = 0;
  gchar        *domain;

  if (! config)
    return NULL;

  vbox = gtk_vbox_new (FALSE, 8);

  table = gtk_table_new (2, 2, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 4);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 8);
  gtk_table_set_col_spacings (GTK_TABLE (table), 4);
  gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
  gtk_widget_show (table);

  label = gtk_label_new (NULL);
  gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_END);
  g_object_set_data (G_OBJECT (lcms), "rgb-profile", label);
  cdisplay_lcms_attach_labelled (GTK_TABLE (table), row++,
                                 _("Image profile:"),
                                 label);
  cdisplay_lcms_update_profile_label (lcms, "rgb-profile");

  label = gtk_label_new (NULL);
  gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_END);
  g_object_set_data (G_OBJECT (lcms), "display-profile", label);
  g_object_set_data (G_OBJECT (lcms), "display-profile-from-gdk", label);
  cdisplay_lcms_attach_labelled (GTK_TABLE (table), row++,
                                 _("Monitor profile:"),
                                 label);
  cdisplay_lcms_update_profile_label (lcms, "display-profile");

  lcms->toggle_use_color_config = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                              "use-color-config",
                                                              _("_Use color management preferences"));
  gtk_box_pack_start (GTK_BOX (vbox), lcms->toggle_use_color_config, FALSE, FALSE, 0);
  gtk_widget_show (lcms->toggle_use_color_config);

  /*************** Proofing controls ***************/
  checkbutton = gimp_prop_check_button_new (G_OBJECT (lcms),
                                            "proofing",
                                            _("_Enable proofing"));
  gtk_widget_show (checkbutton);

  lcms->frame = gtk_frame_new (NULL);
  gtk_frame_set_label_widget (GTK_FRAME (lcms->frame), checkbutton);
  gtk_box_pack_start (GTK_BOX (vbox), lcms->frame, FALSE, FALSE, 0);
  gtk_widget_set_sensitive (lcms->frame, !lcms->use_color_config);

  lcms->table = gtk_table_new (7, 2, FALSE);
  gtk_table_set_col_spacings (GTK_TABLE (lcms->table), 4);
  gtk_container_set_border_width (GTK_CONTAINER (lcms->table), 4);
  gtk_container_add (GTK_CONTAINER (lcms->frame), lcms->table);
  gtk_widget_set_sensitive (lcms->table, lcms->proofing);

  row = 0;

  label = gtk_label_new (_("Target:"));
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (lcms->table), label, 0, 1, row, row + 1, GTK_FILL, GTK_EXPAND, 0, 0);
  label = gtk_label_new (NULL);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_END);
  g_object_set_data (G_OBJECT (lcms), "printer-profile", label);
  gtk_table_attach (GTK_TABLE (lcms->table), label, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);
  cdisplay_lcms_update_profile_label (lcms, "printer-profile");

  row++;

  lcms->toggle_preserve_pixel = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                           "preserve-pixel",
                                                           _("_Preserve pixel value"));
  gtk_widget_set_sensitive (lcms->toggle_preserve_pixel, !lcms->target_is_cmyk);
  gtk_table_attach (GTK_TABLE (lcms->table), lcms->toggle_preserve_pixel, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);

  gtk_table_set_row_spacing (GTK_TABLE (lcms->table), row, 16);
  row++;

  label = gtk_label_new (_("Intent:"));
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (lcms->table), label, 0, 1, row, row + 1, GTK_FILL, GTK_EXPAND, 0, 0);
  lcms->combo_intent = gimp_prop_enum_combo_box_new (G_OBJECT (lcms), "intent", 0, 0);
  gtk_widget_set_sensitive (lcms->combo_intent, !lcms->preserve_pixel);
  gtk_table_attach (GTK_TABLE (lcms->table), lcms->combo_intent, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);

  row++;

  lcms->hbox_intent = gtk_hbox_new (FALSE, 4);
  gtk_widget_set_sensitive (lcms->hbox_intent, !lcms->preserve_pixel);
  gtk_table_attach (GTK_TABLE (lcms->table), lcms->hbox_intent, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);

  lcms->toggle_bpc = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                 "use-bpc",
                                                 _("Use _BPC algorithm"));
  gtk_box_pack_start (GTK_BOX (lcms->hbox_intent), lcms->toggle_bpc, FALSE, FALSE, 0);
  lcms->toggle_chad = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                  "fully-adapted",
                                                  _("Chromatic _adaptation"));
  gtk_widget_set_sensitive (lcms->toggle_chad, lcms->intent == GIMP_COLOR_RENDERING_INTENT_ABSOLUTE_COLORIMETRIC);
  gtk_box_pack_start (GTK_BOX (lcms->hbox_intent), lcms->toggle_chad, FALSE, FALSE, 0);

  gtk_table_set_row_spacing (GTK_TABLE (lcms->table), row, 8);
  row++;

  label = gtk_label_new (_("Simulation:"));
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (lcms->table), label, 0, 1, row, row + 1, GTK_FILL, GTK_EXPAND, 0, 0);

  hbox = gtk_hbox_new (FALSE, 4);
  gtk_table_attach (GTK_TABLE (lcms->table), hbox, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);

  lcms->toggle_black_ink = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                       "simulate-black-ink",
                                                       _("Black point (_K ink)"));
  gtk_widget_set_sensitive (lcms->toggle_black_ink, lcms->channel == CDISPLAY_LCMS_PROOF_CHANNEL_CMYK && !lcms->media_white);
  gtk_box_pack_start (GTK_BOX (hbox), lcms->toggle_black_ink, FALSE, FALSE, 0);
  lcms->toggle_media_white = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                         "simulate-media-white",
                                                         _("Media _white"));
  gtk_widget_set_sensitive (lcms->toggle_media_white, lcms->channel == CDISPLAY_LCMS_PROOF_CHANNEL_CMYK);
  gtk_box_pack_start (GTK_BOX (hbox), lcms->toggle_media_white, FALSE, FALSE, 0);

  gtk_table_set_row_spacing (GTK_TABLE (lcms->table), row, 16);
  row++;

  label = gtk_label_new (_("View:"));
  gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
  gtk_table_attach (GTK_TABLE (lcms->table), label, 0, 1, row, row + 1, GTK_FILL, GTK_EXPAND, 0, 0);

  {
    GtkWidget *vbox2;
    GList *list;

    lcms->hbox_channels = gtk_hbox_new (FALSE, 4);
    gtk_widget_set_sensitive (lcms->hbox_channels, lcms->target_is_cmyk);

    vbox2 = gimp_prop_enum_radio_box_new (G_OBJECT (lcms),"channel", 0, 0);
    list = gtk_container_get_children (GTK_CONTAINER (vbox2));

    while (list)
      {
        GtkWidget *w = (GtkWidget *)list->data;

        g_object_ref (G_OBJECT (w));
        gtk_container_remove (GTK_CONTAINER (vbox2), w);
        gtk_box_pack_start (GTK_BOX (lcms->hbox_channels), w, FALSE, FALSE, 0);

        list = g_list_next (list);
      }

    gtk_table_attach (GTK_TABLE (lcms->table), lcms->hbox_channels, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);
    row++;
  }

  lcms->toggle_grayscale_preview = gimp_prop_check_button_new (G_OBJECT (lcms),
                                                               "grayscale-preview",
                                                               _("Use _grayscale for single plate view"));
  gtk_widget_set_sensitive (lcms->toggle_grayscale_preview, lcms->target_is_cmyk);
  gtk_table_attach (GTK_TABLE (lcms->table), lcms->toggle_grayscale_preview, 1, 2, row, row + 1, GTK_FILL | GTK_EXPAND, GTK_EXPAND, 0, 0);

  lcms->weak_pointer = vbox;
  g_object_add_weak_pointer (G_OBJECT (vbox), &lcms->weak_pointer);

  gtk_widget_show_all (lcms->frame);

  g_signal_connect_object (config, "notify",
                           G_CALLBACK (cdisplay_lcms_notify_profile),
                           lcms, 0);

  return vbox;
}

#ifdef USE_CAIRO_SURFACE
#define RGB_FORMAT (TYPE_ARGB_8)
#define GRAY_FORMAT \
  (COLORSPACE_SH (PT_GRAY) | EXTRA_SH (3) | CHANNELS_SH (1) | BYTES_SH (1))

static void
cdisplay_lcms_convert_surface (GimpColorDisplay *display,
                               cairo_surface_t  *surface)
{
  CdisplayLcms   *lcms   = CDISPLAY_LCMS (display);
  gint            width  = cairo_image_surface_get_width (surface);
  gint            height = cairo_image_surface_get_height (surface);
  gint            stride = cairo_image_surface_get_stride (surface);
  guchar         *buf    = cairo_image_surface_get_data (surface);
  cairo_format_t  fmt    = cairo_image_surface_get_format (surface);
  guchar         *rowbuf;
  gint            x, y;
  guchar          r, g, b, a;

  if (fmt != CAIRO_FORMAT_ARGB32)
    return;

  if (! lcms->transform)
    return;

  rowbuf = g_malloc (stride);

  for (y = 0; y < height; y++, buf += stride)
    {
      /* Switch buf from ARGB premul to ARGB non-premul, since lcms ignores the
       * alpha channel.  The macro takes care of byte order.
       */
      for (x = 0; x < width; x++)
        {
          GIMP_CAIRO_ARGB32_GET_PIXEL (buf + 4*x, r, g, b, a);
          rowbuf[4*x+0] = a;
          rowbuf[4*x+1] = r;
          rowbuf[4*x+2] = g;
          rowbuf[4*x+3] = b;
        }

      cmsDoTransform (lcms->transform, rowbuf, rowbuf, width);

      /* And back to ARGB premul */
      for (x = 0; x < width; x++)
        {
          a = rowbuf[4*x+0];
          r = rowbuf[4*x+1];
          g = rowbuf[4*x+2];
          b = rowbuf[4*x+3];
          GIMP_CAIRO_ARGB32_SET_PIXEL (buf + 4*x, r, g, b, a);
        }
    }

  g_free (rowbuf);
}
#else /* USE_CAIRO_SURFACE */
#define RGB_FORMAT (TYPE_RGB_8)
#define GRAY_FORMAT \
  (COLORSPACE_SH (PT_GRAY) | EXTRA_SH (2) | CHANNELS_SH (1) | BYTES_SH (1))

static void
cdisplay_lcms_convert (GimpColorDisplay *display,
                       guchar           *buf,
                       gint              width,
                       gint              height,
                       gint              bpp,
                       gint              bpl)
{
  CdisplayLcms *lcms = CDISPLAY_LCMS (display);
  gint          y;

  if (bpp != 3)
    return;

  if (! lcms->transform)
    return;

  if (width * bpp != bpl)
    {
      for (y = 0; y < height; y++, buf += bpl)
        cmsDoTransform (lcms->transform, buf, buf, width);
    }
  else
    {
      cmsDoTransform (lcms->transform, buf, buf, width * height);
    }
}
#endif /* USE_CAIRO_SURFACE */

static void
cdisplay_lcms_changed (GimpColorDisplay *display)
{
  CdisplayLcms    *lcms   = CDISPLAY_LCMS (display);
  GimpColorConfig *config = gimp_color_display_get_config (display);

  cmsHPROFILE      src_profile   = NULL;
  cmsHPROFILE      dest_profile  = NULL;
  cmsHPROFILE      proof_profile = NULL;
  LCMSDWORD        flags         = 0;
  LCMSDWORD        src_format;
  LCMSDWORD        dest_format   = RGB_FORMAT;// | DITHER_SH (1);

  gboolean         skip_1st      = FALSE;

  if (lcms->transform)
    {
      cmsDeleteTransform (lcms->transform);
      lcms->transform = NULL;
    }

  if (! config)
    return;

  if (lcms->use_color_config)
    {
      switch (config->mode)
        {
        case GIMP_COLOR_MANAGEMENT_OFF:
          return;

        case GIMP_COLOR_MANAGEMENT_SOFTPROOF:
          proof_profile = cdisplay_lcms_get_printer_profile (lcms);
          /*  fallthru  */

        case GIMP_COLOR_MANAGEMENT_DISPLAY:
          src_profile = cdisplay_lcms_get_rgb_profile (lcms);
          dest_profile = cdisplay_lcms_get_display_profile (lcms);
          break;
        }

      if (config->display_intent ==
          GIMP_COLOR_RENDERING_INTENT_RELATIVE_COLORIMETRIC)
        {
          flags |= cmsFLAGS_BLACKPOINTCOMPENSATION;
        }

      if (proof_profile)
        {
          if (! src_profile)
            src_profile = cmsCreate_sRGBProfile ();

          if (! dest_profile)
            dest_profile = cmsCreate_sRGBProfile ();

          flags |= cmsFLAGS_SOFTPROOFING;

          if (config->simulation_gamut_check)
            {
              guchar r, g, b;

              flags |= cmsFLAGS_GAMUTCHECK;

              gimp_rgb_get_uchar (&config->out_of_gamut_color, &r, &g, &b);

#ifdef HAVE_LCMS1
              cmsSetAlarmCodes (r, g, b);
#else
              {
                cmsUInt16Number  alarmCodes[cmsMAXCHANNELS] = { 0 };
                alarmCodes[0] = (cmsUInt16Number) r;
                alarmCodes[1] = (cmsUInt16Number) g;
                alarmCodes[2] = (cmsUInt16Number) b;

                cmsSetAlarmCodes (alarmCodes);
              }
#endif
            }

          lcms->transform = cmsCreateProofingTransform (src_profile,  RGB_FORMAT,
                                                        dest_profile, RGB_FORMAT,
                                                        proof_profile,
                                                        config->simulation_intent,
                                                        config->display_intent,
                                                        flags);
          cmsCloseProfile (proof_profile);
        }
      else if (src_profile || dest_profile)
        {
          if (! src_profile)
            src_profile = cmsCreate_sRGBProfile ();

          if (! dest_profile)
            dest_profile = cmsCreate_sRGBProfile ();

          lcms->transform = cmsCreateTransform (src_profile,  RGB_FORMAT,
                                                dest_profile, RGB_FORMAT,
                                                config->display_intent,
                                                flags);
        }

      if (dest_profile)
        cmsCloseProfile (dest_profile);

      if (src_profile)
        cmsCloseProfile (src_profile);

      return;
    }

  if (lcms->proofing)
    {
      proof_profile = cdisplay_lcms_get_printer_profile (lcms);

      if (proof_profile &&
          lcms->preserve_pixel &&
          cmsGetColorSpace (proof_profile) == cmsSigRgbData)
        skip_1st = TRUE;
    }

  if (!skip_1st)
    src_profile = cdisplay_lcms_get_rgb_profile (lcms);

  dest_profile = cdisplay_lcms_get_display_profile (lcms);

  cmsSetAdaptationState (0.);

  if (proof_profile)
    {
      cmsHPROFILE profiles[4];
      cmsHTRANSFORM tmp;
      GimpColorRenderingIntent intent; /* intent for stage 2 */
      gboolean black_ink, media_white;
      gboolean target_is_cmyk;
      gint index = 0;

      if (! src_profile)
       src_profile = cmsCreate_sRGBProfile ();

      if (! dest_profile)
       dest_profile = cmsCreate_sRGBProfile ();

      src_format = cmsGetColorSpace (src_profile) == cmsSigGrayData ?
        GRAY_FORMAT : RGB_FORMAT;

      /***** stage 1 *********************************************/

      if (!skip_1st)
        {
          flags = lcms->use_bpc ? cmsFLAGS_BLACKPOINTCOMPENSATION : 0;

          if (lcms->fully_adapted)
            {
              flags |= cmsFLAGS_NOWHITEONWHITEFIXUP;
              cmsSetAdaptationState (1.0);
            }

          tmp = cmsCreateTransform (src_profile, src_format,
                                    proof_profile, COLORSPACE_SH (PT_ANY) | CHANNELS_SH (1) | BYTES_SH (1),
                                    lcms->intent,
                                    flags);

#ifdef HAVE_LCMS1
          profiles[index++] = cmsTransform2DeviceLink (tmp, flags);
#else
          profiles[index++] = cmsTransform2DeviceLink (tmp, 2.1, flags);
#endif
        }

      /***** stage 2 *********************************************/

      /* selecting C/M/Y/K channel */
      target_is_cmyk = cmsGetColorSpace (proof_profile) == cmsSigCmykData &&
                       cmsGetDeviceClass (proof_profile) == cmsSigOutputClass;

      if (target_is_cmyk)
        {
          profiles[index] = lcms->grayscale_preview ? preview2[lcms->channel] : preview1[lcms->channel];

          if (cmsGetPCS (profiles[index++]) == cmsSigCmykData)
            profiles[index++] = proof_profile;
        }
      else
        profiles[index++] = proof_profile;

      profiles[index++] = dest_profile;

      /* disable radio buttons if target profile is not CMYK */
      if (lcms->weak_pointer)
        {
          gtk_widget_set_sensitive (lcms->hbox_channels, target_is_cmyk);
          gtk_widget_set_sensitive (lcms->toggle_grayscale_preview, target_is_cmyk);
          gtk_widget_set_sensitive (lcms->toggle_preserve_pixel, !target_is_cmyk);
        }
      if (lcms->target_is_cmyk && !target_is_cmyk)
        {
          lcms->update = FALSE;
          g_object_set (G_OBJECT (lcms), "channel", CDISPLAY_LCMS_PROOF_CHANNEL_CMYK, NULL);
          lcms->update = TRUE;
        }
      else if (!lcms->target_is_cmyk && target_is_cmyk)
        {
          lcms->update = FALSE;
          g_object_set (G_OBJECT (lcms), "preserve-pixel", FALSE, NULL);
          lcms->update = TRUE;
        }

      lcms->target_is_cmyk = target_is_cmyk;

      if (lcms->channel < CDISPLAY_LCMS_PROOF_CHANNEL_CMYK)
        {
          black_ink = TRUE;
          media_white = FALSE;
        }
      else
        {
          black_ink = lcms->black_ink;
          media_white = lcms->media_white;
        }

      flags = black_ink ? 0 : cmsFLAGS_BLACKPOINTCOMPENSATION;

      cmsSetAdaptationState (0.);

      if (media_white)
        {
          intent = INTENT_ABSOLUTE_COLORIMETRIC;
          flags |= cmsFLAGS_NOWHITEONWHITEFIXUP;

          /* 色順応の使用をproof_profileのデバイスクラスをもとに決めているが、 *
           * ユーザーに選択させるべきかどうかの判断を保留中                    */
          if (cmsGetDeviceClass (proof_profile) == cmsSigOutputClass &&
              cmsIsTag (dest_profile, 0x63686164L/*'chad'*/) == FALSE)
            cmsSetAdaptationState (1.0);
        }
      else
        intent = INTENT_RELATIVE_COLORIMETRIC;

      if (skip_1st)
        {
          lcms->transform = cmsCreateTransform (proof_profile, RGB_FORMAT,
                                                dest_profile, dest_format,
                                                intent, flags);
        }
      else
        {
          lcms->transform = cmsCreateMultiprofileTransform (profiles, index,
                                                            src_format, dest_format,
                                                            intent, flags);

          cmsDeleteTransform (tmp);
          cmsCloseProfile (profiles[0]);
        }

      cmsCloseProfile (proof_profile);
    }
  else if (src_profile || dest_profile)
    {
      if (! src_profile)
       src_profile = cmsCreate_sRGBProfile ();

      if (! dest_profile)
       dest_profile = cmsCreate_sRGBProfile ();

      src_format = cmsGetColorSpace (src_profile) == cmsSigGrayData ?
        GRAY_FORMAT : RGB_FORMAT;

      lcms->transform = cmsCreateTransform (src_profile,  src_format,
                                            dest_profile, dest_format,
                                            INTENT_RELATIVE_COLORIMETRIC,
                                            flags);
    }

  if (dest_profile)
    cmsCloseProfile (dest_profile);

  if (src_profile)
    cmsCloseProfile (src_profile);
}

static gboolean
cdisplay_lcms_profile_is_rgb (cmsHPROFILE profile)
{
  return (cmsGetColorSpace (profile) == cmsSigRgbData);
}

static gboolean
cdisplay_lcms_profile_is_valid (cmsHPROFILE profile)
{
  cmsColorSpaceSignature colorspace = cmsGetColorSpace (profile);

  return (colorspace == cmsSigRgbData || colorspace == cmsSigGrayData);
}

static cmsHPROFILE
cdisplay_lcms_get_rgb_profile (CdisplayLcms *lcms)
{
  GimpColorConfig  *config;
  GimpColorManaged *managed;
  cmsHPROFILE       profile = NULL;

  managed = gimp_color_display_get_managed (GIMP_COLOR_DISPLAY (lcms));

  if (managed)
    {
      gsize         len;
      const guint8 *data = gimp_color_managed_get_icc_profile (managed, &len);

      if (data)
        profile = cmsOpenProfileFromMem ((gpointer) data, len);

      if (profile &&
          ! cdisplay_lcms_profile_is_valid (profile))
        {
          cmsCloseProfile (profile);
          profile = NULL;
        }
    }

  if (! profile)
    {
      config = gimp_color_display_get_config (GIMP_COLOR_DISPLAY (lcms));

      if (config->rgb_profile)
        profile = cmsOpenProfileFromFile (config->rgb_profile, "r");
    }

  return profile;
}

static GdkScreen *
cdisplay_lcms_get_screen (CdisplayLcms *lcms,
                          gint         *monitor)
{
  GimpColorManaged *managed;
  GdkScreen        *screen;

  managed = gimp_color_display_get_managed (GIMP_COLOR_DISPLAY (lcms));

  if (GTK_IS_WIDGET (managed))
    screen = gtk_widget_get_screen (GTK_WIDGET (managed));
  else
    screen = gdk_screen_get_default ();

  g_return_val_if_fail (GDK_IS_SCREEN (screen), NULL);

  if (GTK_IS_WIDGET (managed) && gtk_widget_is_drawable (GTK_WIDGET (managed)))
    {
      GtkWidget *widget = GTK_WIDGET (managed);

      *monitor = gdk_screen_get_monitor_at_window (screen,
                                                   gtk_widget_get_window (widget));
    }
  else
    {
      *monitor = 0;
    }

  return screen;
}


static cmsHPROFILE
cdisplay_lcms_get_display_profile (CdisplayLcms *lcms)
{
  GimpColorConfig *config;
  cmsHPROFILE      profile = NULL;

  config = gimp_color_display_get_config (GIMP_COLOR_DISPLAY (lcms));

#if defined GDK_WINDOWING_X11
  if (config->display_profile_from_gdk)
    {
      GdkScreen *screen;
      GdkAtom    type    = GDK_NONE;
      gint       format  = 0;
      gint       nitems  = 0;
      gint       monitor = 0;
      gchar     *atom_name;
      guchar    *data    = NULL;

      screen = cdisplay_lcms_get_screen (lcms, &monitor);

      if (monitor > 0)
        atom_name = g_strdup_printf ("_ICC_PROFILE_%d", monitor);
      else
        atom_name = g_strdup ("_ICC_PROFILE");

      if (gdk_property_get (gdk_screen_get_root_window (screen),
                            gdk_atom_intern (atom_name, FALSE),
                            GDK_NONE,
                            0, 64 * 1024 * 1024, FALSE,
                            &type, &format, &nitems, &data) && nitems > 0)
        {
          profile = cmsOpenProfileFromMem (data, nitems);
          g_free (data);
        }

      g_free (atom_name);
    }

#elif defined GDK_WINDOWING_QUARTZ
  if (config->display_profile_from_gdk)
    {
      CMProfileRef  prof    = NULL;
      gint          monitor = 0;

      cdisplay_lcms_get_screen (lcms, &monitor);

      CMGetProfileByAVID (monitor, &prof);

      if (prof)
        {
          CFDataRef data;

          data = CMProfileCopyICCData (NULL, prof);
          CMCloseProfile (prof);

          if (data)
            {
              UInt8 *buffer = g_malloc (CFDataGetLength (data));

              /* We cannot use CFDataGetBytesPtr(), because that returns
               * a const pointer where cmsOpenProfileFromMem wants a
               * non-const pointer.
               */
              CFDataGetBytes (data, CFRangeMake (0, CFDataGetLength (data)),
                              buffer);

              profile = cmsOpenProfileFromMem (buffer, CFDataGetLength (data));

              g_free (buffer);
              CFRelease (data);
            }
        }
    }

#elif defined G_OS_WIN32
  if (config->display_profile_from_gdk)
    {
      HDC hdc = GetDC (NULL);

      if (hdc)
        {
          gchar *path;
          DWORD  len = 0;

          GetICMProfile (hdc, &len, NULL);
          path = g_new (gchar, len);

          if (GetICMProfile (hdc, &len, path))
            profile = cmsOpenProfileFromFile (path, "r");

          g_free (path);
          ReleaseDC (NULL, hdc);
        }
    }
#endif

  if (! profile && config->display_profile)
    profile = cmsOpenProfileFromFile (config->display_profile, "r");

  return profile;
}

static cmsHPROFILE
cdisplay_lcms_get_printer_profile (CdisplayLcms *lcms)
{
  GimpColorConfig *config;

  config = gimp_color_display_get_config (GIMP_COLOR_DISPLAY (lcms));

  if (config->printer_profile)
    return cmsOpenProfileFromFile (config->printer_profile, "r");

  return NULL;
}

static void
cdisplay_lcms_attach_labelled (GtkTable    *table,
                               gint         row,
                               const gchar *text,
                               GtkWidget   *widget)
{
  GtkWidget *label;

  label = g_object_new (GTK_TYPE_LABEL,
                        "label",  text,
                        "xalign", 1.0,
                        "yalign", 0.5,
                        NULL);

  gimp_label_set_attributes (GTK_LABEL (label),
                             PANGO_ATTR_WEIGHT, PANGO_WEIGHT_BOLD,
                             -1);
  gtk_table_attach (table, label, 0, 1, row, row + 1, GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show (label);

  if (GTK_IS_LABEL (widget))
    gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);

  gtk_table_attach (table, widget, 1, 2, row, row + 1,
                    GTK_FILL | GTK_EXPAND, GTK_FILL, 0, 0);
  gtk_widget_show (widget);
}

static void
cdisplay_lcms_update_profile_label (CdisplayLcms *lcms,
                                    const gchar  *name)
{
  GtkWidget   *label;
  cmsHPROFILE  profile = NULL;
#ifdef HAVE_LCMS1
  const gchar *text;
  const gchar *tooltip;
#else
  gchar       *text    = NULL;
  gchar       *tooltip = NULL;
#endif

  label = g_object_get_data (G_OBJECT (lcms), name);

  if (! label)
    return;

  if (strcmp (name, "rgb-profile") == 0)
    {
      profile = cdisplay_lcms_get_rgb_profile (lcms);
    }
  else if (g_str_has_prefix (name, "display-profile"))
    {
      profile = cdisplay_lcms_get_display_profile (lcms);
    }
  else if (strcmp (name, "printer-profile") == 0)
    {
      profile = cdisplay_lcms_get_printer_profile (lcms);
    }
  else
    {
      g_return_if_reached ();
    }

  cdisplay_lcms_profile_get_info (profile, &text, &tooltip);

  gtk_label_set_text (GTK_LABEL (label), text);
  gimp_help_set_help_data (label, tooltip, NULL);

#ifdef HAVE_LCMS2
  g_free (text);
  g_free (tooltip);
#endif

  if (profile)
    cmsCloseProfile (profile);
}

static void
cdisplay_lcms_notify_profile (GObject      *config,
                              GParamSpec   *pspec,
                              CdisplayLcms *lcms)
{
  cdisplay_lcms_update_profile_label (lcms, pspec->name);
}
